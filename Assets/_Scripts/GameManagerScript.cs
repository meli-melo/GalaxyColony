﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class GameManagerScript : MonoBehaviour
{
	private Time _gameStartTime;
	public Island _playerIsland;
	public Text dataText;
	public Text _islandInfoText;
	public ResourcesManagerScript _resourceManager;

	// Use this for initialization
	void Start ()
	{
		//_gameStartTime = Time.
		_playerIsland = new Island();
		// Debug.Log("island's size: " + _playerIsland.getSize());
		// Debug.Log("island's wood quantity: " + _playerIsland.getWoodQuantity());
		// Debug.Log("island's food quantity: " + _playerIsland.getFoodQuantity());
		// Debug.Log("island's ore quantity: " + _playerIsland.getOreQuantity());
		_islandInfoText.text = "Island slots: " + _playerIsland.getOccupatedSpace() + "/" + _playerIsland.getSize();
		_resourceManager.initResources();
		//_resourceManager.setIsland(_playerIsland);
	}
	
	// Update is called once per frame
	void Update ()
	{
		_islandInfoText.text = "Island slots: " + _playerIsland.getOccupatedSpace() + "/" + _playerIsland.getSize();
		//get the list of buildings on the island qnd displays the level of the mine
		if(Input.GetKeyDown(KeyCode.Space))
		{
			Debug.Log(" SPACE ");
			List<CollectingEntity> islandBuildings = _playerIsland.getBuildingsList();
			Debug.Log("count " + islandBuildings.Count);
			foreach(CollectingEntity building in islandBuildings)
			{
				if(building is Mine)
					Debug.Log("Mine " + building.getBuildingLevel());
			}
		}
	}

	public void displayGameData()
	{
		List<CollectingEntity> islandBuildings = _playerIsland.getBuildingsList();
		string dataToDisplay = "count : " + islandBuildings.Count + "\n";
		foreach(CollectingEntity building in islandBuildings)
		{
			if(building is Mine)
			{
				dataToDisplay += "Mine " + building.getBuildingLevel() + "\n";
			}

			if(building is Farm)
			{
				dataToDisplay += "Farm " + building.getBuildingLevel() + "\n";
			}
		}
		dataText.text = dataToDisplay;
	}

	// public void UpdateIslandData()
	// {
	// 	_playerIsland.updateIslandBuildings();
	// }

	//called when mine button is pressed
	public void CreateUpgradeMine()
	{
		Debug.Log("create upgrade Mine");
		Mine islandMine = _playerIsland.CreateUpgradeMine();
		if(islandMine.getMineLevel() == 1)
		{
			_resourceManager.setMine(islandMine);
		}
	}

	//called when farm button is pressed
	public void CreateUpgradeFarm()
	{
		Debug.Log("create upgrade Farm");
		Farm islandFarm = _playerIsland.CreateUpgradeFarm();
		if(islandFarm.getFarmLevel() == 1)
		{
			_resourceManager.setFarm(islandFarm);
		}
	}
}