# Galaxy Colony

Galaxy Colony is a Sci-Fi mobile game where the player has to manage resources to colonize a planet and build a base.

## Getting Started

Download the repository and open it in Unity 5.3 or higher

### Prerequisites

You need to install Unity 5.3 or higher and the Android development tools that are required to build with Unity.

### Installing

To install the game you just need to build it using Unity build window. It will either install directly the game to your mobile device or create a .apk file that you can then copy manually to your device and then run it to install the game.

## Authors

* **Jean Bourquard** - *Whole project* - [meli-melo](https://gitlab.com/meli-melo)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details