using UnityEngine;
using System.Collections;

public class Farm : CollectingEntity
{
	public Farm() : base()
	{
		_resourceTypeCollected = "Food";
	}

	public void upgradeFarm()
	{
		base.upgradeCollectingEntity();
	}

	public int getFarmLevel()
	{
		return base.getBuildingLevel();
	}

	public int getFoodCollectedPerHour()
	{
		return base.getResourcesCollecterPerHour();
	}
}
