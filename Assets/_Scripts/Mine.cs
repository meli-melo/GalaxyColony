﻿using UnityEngine;
using System.Collections;

public class Mine : CollectingEntity
{
	public Mine() : base()
	{
		_resourceTypeCollected = "Ore";
	}

	public void upgradeMine()
	{
		base.upgradeCollectingEntity();
	}

	public int getMineLevel()
	{
		return base.getBuildingLevel();
	}

	public int getOreCollectedPerHour()
	{
		return base.getResourcesCollecterPerHour();
	}
}
