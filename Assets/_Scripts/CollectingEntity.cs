﻿using UnityEngine;
using System.Collections;

public class CollectingEntity
{
	private int _buildingSpace;
	private int _resourceCollectedPerHour;
	private int _buildingUpgradeCost;
	private int _energyConsumption;
	private int _buildingLevel;
	protected string _resourceTypeCollected;

	public CollectingEntity()
	{
		_buildingSpace = 1;
		_resourceCollectedPerHour = 60;
		_buildingUpgradeCost = 100;
		_energyConsumption = 10;
		_buildingLevel = 1;
	}

	protected void upgradeCollectingEntity()
	{
		_buildingSpace ++;
		_buildingLevel ++;
		_buildingUpgradeCost *= 2;
		_resourceCollectedPerHour += (_buildingLevel*10);
		_energyConsumption *= 2;
	}

	public int getBuildingLevel()
	{
		return _buildingLevel;
	}

	public int getResourcesCollecterPerHour()
	{
		return _resourceCollectedPerHour;
	}
}
