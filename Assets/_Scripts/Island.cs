﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Island
{
	private int _size;
	private int _woodQuantity;
	private int _oreQuantity;
	private int _foodQuantity;
	private int _occupatedSpace;
	private List<CollectingEntity> _buildings = new List<CollectingEntity>();

	// Use this for initialization
	public Island ()
	{
		_size = Random.Range(50, 250);
		_woodQuantity = _size * Random.Range(10, 20);
		_oreQuantity = _size * Random.Range(10, 15);
		_foodQuantity = _size * Random.Range(5, 10);
		_occupatedSpace = 0;
	}

	public int getSize()
	{
		return _size;
	}

	public int getWoodQuantity()
	{
		return _woodQuantity;
	}

	public int getFoodQuantity()
	{
		return _foodQuantity;
	}

	public int getOreQuantity()
	{
		return _oreQuantity;
	}

	public void updateIslandBuildings()
	{
		_occupatedSpace++;
	}

	public int getOccupatedSpace()
	{
		return _occupatedSpace;
	}

	public Mine CreateUpgradeMine()
	{
		bool upgradeBuilding = false;
		int buildingIndex = -1;

		//check if there is already a mine existing on the island
		for(int i = 0; i < _buildings.Count; i++)
		{
			if(_buildings[i] is Mine)
			{
				upgradeBuilding = true;
				buildingIndex = i;
				break;
			}
		}

		//if there is already a mine on the island, upgrade it
		if(upgradeBuilding)
		{
			Mine mineToUpgrade = _buildings[buildingIndex] as Mine;
			mineToUpgrade.upgradeMine();
			_occupatedSpace ++;
			return mineToUpgrade;
		}
		else //else create a new one
		{
			Mine mineToAdd = new Mine();
			_buildings.Add(mineToAdd);
			_occupatedSpace ++;
			return mineToAdd;
		}		
	}

	public Farm CreateUpgradeFarm()
	{
		bool upgradeBuilding = false;
		int buildingIndex = -1;

		//check if there is already a mine existing on the island
		for(int i = 0; i < _buildings.Count; i++)
		{
			if(_buildings[i] is Farm)
			{
				upgradeBuilding = true;
				buildingIndex = i;
				break;
			}
		}

		//if there is already a mine on the island, upgrade it
		if(upgradeBuilding)
		{
			Farm farmToUpgrade = _buildings[buildingIndex] as Farm;
			farmToUpgrade.upgradeFarm();
			_occupatedSpace ++;
			return farmToUpgrade;
		}
		else //else create a new one
		{
			Farm farmToAdd = new Farm();
			_buildings.Add(farmToAdd);
			_occupatedSpace ++;
			return farmToAdd;
		}		
	}

	public List<CollectingEntity> getBuildingsList()
	{
		return _buildings;
	} 
}