﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ResourcesManagerScript : MonoBehaviour
{
	public Text _woodResourcesText;
	public Text _foodResourcesText;
	public Text _oreResourcesText;

	private int _woodResources;
	private int _foodResources;
	private int _oreResources;

	private Mine _playerMine = null;
	private Farm _playerFarm = null;

	private float _minuteTimer = 0;
	private float _oreCollectedBuffer = 0;
	private float _foodCollectedBuffer = 0;

	// Use this for initialization
	// void Start ()
	// {
		
	// }

	// public void setIsland(Island playerIsland)
	// {
	// 	_playerIsland = playerIsland;
	// }

	//initial resources the player has when he starts a game
	public void initResources()
	{
		_woodResources = 500;
		_foodResources = 500;
		_oreResources = 500;
	}
	
	// Update is called once per frame
	void Update ()
	{
		// if(_playerMine != null)
		// 	Debug.Log("Mine resource " + _playerMine.getMineLevel());
		calculateResources();
		updateResourceUI();
	}

	//calculate the new resources amounts every minutes
	private void calculateResources()
	{
		_minuteTimer += Time.deltaTime;
		if(_minuteTimer >= 60f)
		{
			Debug.Log("minute");
			//update the ore collected
			_oreCollectedBuffer += _playerMine.getOreCollectedPerHour() / 60f;
			if(_oreCollectedBuffer >= 1)
			{
				_oreResources += Mathf.FloorToInt(_oreCollectedBuffer);
				_oreCollectedBuffer -= Mathf.Floor(_oreCollectedBuffer);
			}

			//update the food collected
			_foodCollectedBuffer += _playerFarm.getFoodCollectedPerHour() / 60f;
			if(_foodCollectedBuffer >= 1)
			{
				_foodResources += Mathf.FloorToInt(_foodCollectedBuffer);
				_foodCollectedBuffer -= Mathf.Floor(_foodCollectedBuffer);
			}
			_minuteTimer -= 60f;
		}
	}

	//updates the resource UI with the right values
	private void updateResourceUI()
	{
		_woodResourcesText.text = _woodResources.ToString();
		_foodResourcesText.text = _foodResources.ToString();
		_oreResourcesText.text = _oreResources.ToString();
	}

	//called to set the mine at its creation
	public void setMine(Mine updateMine)
	{
		_playerMine = updateMine;
	}

	//called to set the farm at its creation
	public void setFarm(Farm updateFarm)
	{
		_playerFarm = updateFarm;
	}
}
